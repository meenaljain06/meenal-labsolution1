package com.greatlearning.lab.solution;

import com.greatlearning.lab.solution.model.Employee;
import com.greatlearning.lab.solution.service.CredentialService;

public class LabSolutionApp {
    public static void main(String[] args){
        Employee employee = new Employee("Harshit", "Choudary");
        CredentialService credentialService = new CredentialService(employee.getFirstName(), employee.getLastName());
        credentialService.showInfo();
    }
}
