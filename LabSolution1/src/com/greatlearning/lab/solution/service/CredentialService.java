package com.greatlearning.lab.solution.service;

import java.util.Random;
import java.util.Scanner;

public class CredentialService {

    private String firstName;
    private String lastName;
    private String department;
    private String email;
    private String password;
    private int defaultpasswordlength = 8;

    public CredentialService(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;

        this.department = setDepartment();
        this.email = generateEmailAddress(firstName, lastName);
        this.password = getPassword(defaultpasswordlength);
    }

    public String generateEmailAddress(String firstName, String lastName) {
        String companySuffix = "abc.com";
        return firstName.toLowerCase() + lastName.toLowerCase() + "@" + department + "." + companySuffix;
    }

    private String getPassword(int length) {
        String capitalLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String smallLetters = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
        String specialCharacters = "!@#$%^&*_=+-/.?<>)";
        String values = capitalLetters + smallLetters + numbers + specialCharacters;
        Random random = new Random();
        char[] password = new char[8];

        for (int i = 0; i < 8; ++i) {
            password[i] = values.charAt(random.nextInt(values.length()));
        }

        return new String(password);

    }

    public void showInfo() {

        System.out.println("Dear " + firstName + " your generated credentials are as follows \n ");

        System.out.println("Email: " + email);

        System.out.println("Password :" + this.password);

    }

    private String setDepartment() {
        System.out.println("Please enter the department from the following:\n1. Technical \n2. Admin \n3. Human Resource \n4. Legal");
        Scanner sc = new Scanner(System.in);
        int depchoice = sc.nextInt();
        if (depchoice == 1) {
            return "tech";
        } else if (depchoice == 2) {
            return "admin";
        } else if (depchoice == 3) {
            return "hr";
        } else {
            return "lgl";
        }
    }
}
